package dao;

import java.util.ArrayList;

public class Dao {
	
	public static ArrayList<String> ui = new ArrayList<String>();
	public static String active = "";
	public static String path = "";
	
	public static void addUi(String name) {
		if(!ui.contains(name)) {
			ui.add(name);
		}
	}
	
	public static void setActive(String name) {
		active = name;
	}

	public static ArrayList<String> getUi() {
		return new ArrayList<String>(ui);
	}

	public static String getActive() {
		return active;
	}

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		Dao.path = path;
	}
	
	public static void removeUi(String name) {
		if(ui.contains(name)) {
			ui.remove(name);
		}
	}
	
}
