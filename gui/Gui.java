package gui;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import service.Services;
import dao.Dao;

@SuppressWarnings("serial")
public class Gui extends JFrame {
	private Controller controller = new Controller();
	private JButton btnAddUi;
	private JButton btnRemoveUi;
	private JButton btnActivate;
	private JLabel lblUi;
	private JComboBox<String> comboBox;
	private JButton btnChoosePath;
	private JTextField txtUiName;
	private JButton btnSave;
	private JLabel lblCurrentPath;
	private JLabel lblPath;

	public Gui() {
		// Frame setup
		this.setSize(280, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("wow UI switcher");
		this.setLocation(400, 200);
		this.setResizable(false);
		getContentPane().setLayout(null);

		comboBox = new JComboBox<String>(Dao.getUi().toArray(new String[0]));
		comboBox.setBounds(10, 11, 158, 26);
		getContentPane().add(comboBox);

		btnAddUi = new JButton("Add UI");
		btnAddUi.setBounds(178, 44, 89, 23);
		btnAddUi.addActionListener(controller);
		getContentPane().add(btnAddUi);

		btnRemoveUi = new JButton("Remove UI");
		btnRemoveUi.setBounds(178, 78, 89, 23);
		btnRemoveUi.addActionListener(controller);
		getContentPane().add(btnRemoveUi);

		btnActivate = new JButton("Active UI");
		btnActivate.setToolTipText("Activates the UI selected in the dropdown");
		btnActivate.setBounds(178, 112, 89, 23);
		btnActivate.addActionListener(controller);
		getContentPane().add(btnActivate);

		JLabel lblCurrent = new JLabel("Current UI:");
		lblCurrent.setBounds(10, 82, 95, 14);
		getContentPane().add(lblCurrent);

		lblUi = new JLabel(Dao.getActive());
		lblUi.setBounds(10, 107, 95, 14);
		getContentPane().add(lblUi);
		
		btnChoosePath = new JButton("Choose Path");
		btnChoosePath.setToolTipText("Choose the path to your World of Warcraft installation");
		btnChoosePath.setBounds(178, 13, 89, 23);
		btnChoosePath.setMargin(new Insets(0, 0, 0, 0));
		btnChoosePath.addActionListener(controller);
		getContentPane().add(btnChoosePath);
		
		txtUiName = new JTextField();
		txtUiName.setToolTipText("name of the UI you wish to add to the list");
		txtUiName.setText("Insert UI name here");
		txtUiName.setBounds(10, 42, 158, 26);
		getContentPane().add(txtUiName);
		txtUiName.setColumns(10);
		
		btnSave = new JButton("Save");
		btnSave.setBounds(79, 238, 89, 23);
		btnSave.addActionListener(controller);
		getContentPane().add(btnSave);
		
		lblCurrentPath = new JLabel("Current Path:");
		lblCurrentPath.setBounds(10, 150, 77, 14);
		getContentPane().add(lblCurrentPath);
		
		lblPath = new JLabel(Dao.getPath());
		lblPath.setBounds(10, 175, 46, 14);
		getContentPane().add(lblPath);
	}

	private class Controller implements ActionListener {

		@SuppressWarnings("static-access")
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource().equals(btnActivate)) {
				System.out.println("test");
				Services.rename((String) comboBox.getSelectedItem());
				Services.setActive((String) comboBox.getSelectedItem());
				lblUi.setText(Dao.getActive());
				System.out.println(Dao.getPath()+Dao.getActive());
			} else if(e.getSource().equals(btnChoosePath)) {
				JFileChooser jfc = new JFileChooser();
				jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnvlv = jfc.showDialog(null, "Select");
				if(returnvlv == jfc.APPROVE_OPTION) {
					Services.setPath(jfc.getSelectedFile().toString());
					System.out.println(Dao.getPath());
				}
				lblPath.setText(Dao.getPath());
				
			} else if(e.getSource().equals(btnAddUi)) {
				Services.addUi(txtUiName.getText());
				comboBox.removeAllItems();
				for(String s:Dao.getUi()) {
					comboBox.addItem(s);
				}
			} else if(e.getSource().equals(btnRemoveUi)) {
				Services.removeUi((String) comboBox.getSelectedItem());
				comboBox.removeAllItems();
				for(String s:Dao.getUi()) {
					comboBox.addItem(s);
				}
			} else if(e.getSource().equals(btnSave)) {
				Services.saveData();
			}

		}
	}
}