package renamerV2;

import java.util.ArrayList;

public class Dao {
	
	public static ArrayList<String> ui = new ArrayList<String>();
	public static String active;

	public static void addUi(String name) {
		if(!ui.contains(name)) {
			ui.add(name);
		}
	}
	
	public static void setActive(String name) {
		active = name;
	}

	public static ArrayList<String> getUi() {
		return new ArrayList<String>(ui);
	}

	public static String getActive() {
		return active;
	}
	
}
