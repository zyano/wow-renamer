package renamerV2;

import gui.Gui;
import service.Services;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Services.loadData();
		Gui gui = new Gui();
		gui.setVisible(true);
	}
}