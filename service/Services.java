package service;
import java.io.IOException;

import model.Filechanger;
import model.Loader;
import dao.Dao;

public class Services {
	
	public static void rename(String name) {
		System.out.println(Dao.getActive());
		System.out.println(Dao.getPath());
		Filechanger fc = new Filechanger(Dao.getActive(),Dao.getPath());
		fc.rename(name);
		Dao.setActive(name);
	}
	
	public static void loadData() {
		Loader l = new Loader();
		l.loadData();
	}
	
	public static void addUi(String name) {
		Dao.addUi(name);
	}
	
	public static void setPath(String path) {
		Dao.setPath(path);
	}
	
	public static void setActive(String name) {
		Dao.setActive(name);
	}
	
	public static void removeUi(String name) {
		Dao.removeUi(name);
	}
	
	public static void saveData() {
		Loader l = new Loader();
		try {
			l.saveData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
