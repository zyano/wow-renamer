package model;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

import dao.Dao;



public class Loader {
	private Scanner scanner;

	/**
	 * @param args
	 * @throws IOException 
	 */
	public void saveData() throws IOException {

		PrintWriter writer = null;
		
		try {
			writer = new PrintWriter(Dao.getPath()+"\\Settings.ini");
			ArrayList<String> temp = new ArrayList<String>(Dao.getUi());
			for(String s:temp) {
				writer.println("[UI]"+s);
			}
			writer.println("[Active]"+Dao.getActive());
			writer.println("[Path]"+Dao.getPath());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			writer.flush();
			writer.close();
		}
	}
	
	public void loadData() {
		scanner = null;
		
		try {
			scanner = new Scanner(new BufferedReader(new FileReader("C:\\test\\settings.ini")));
			String next = "";
			while (scanner.hasNext()) {
				next = scanner.next();
				System.out.println(next);
				if(next.contains("[UI]")) {
					Dao.addUi(next.substring(4));
				} else if(next.contains("[Active]")) {
					Dao.setActive(next.substring(8));
				} else if(next.contains("[Path]")) {
					Dao.setPath(next.substring(6));
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
}