package model;
/**
 * <h1>File Changer Class </h1>
 * @author Stefan Weibel
 * @version 2
 * @note work in progress
 */
import java.io.File;

public class Filechanger extends Object {
	private File path;
	private String current;
	
	/**
	 * @param String string with the name of the currently active UI could be warlock.
	 * @param File The path to the root folder in the form a File object.
	 */
	public Filechanger(String current, String path) {
		this.path = new File(path);
		this.current = current;
	}
	
	public Filechanger() {
		this.path = null;
		this.current = "";
	}

	/**
	 * Used to get the currently selected path.
	 * @return Returns a file object containing the information about the path.
	 */
	public File getPath() {
		return path;
	}

	/**
	 * Sets the path to a new one.
	 * @param File Takes a file object with a path and sets it as path.
	 */
	public void setPath(File path) {
		this.path = path;
	}

	/**
	 * Used to obtain the name of the active UI
	 * @return Returns the active UI as a string object.
	 */
	public String getCurrent() {
		return current;
	}

	/**
	 * Sets a new current UI should only be used if the constructor is giving a wrong current
	 * @param String Takes a string object with the name of the currently active UI
	 */
	public void setCurrent(String current) {
		this.current = current;
	}
	
	/**
	 * 
	 * @param String String with the name addition to the folders eg. Warlock
	 * @return True if the rename was a success it can return true if only parts of the rename was a success
	 * 
	 */
	public boolean rename(String str) {
		
		// checking if the rename was a success
		boolean success = false;
		
		// Currently active Inteface folder
		File currentInterface = new File(path,"Interface "+current); 
		File newCurrentInterface = new File(path, "Interface "+str);
		File wowInterface = new File(path,"Interface");
		
		/**
		 * Creates file objects for the renaming process of the WTF folder.
		 */
		File currentWTF = new File(path,"WTF "+current); 
		File newCurrentWTF = new File(path, "WTF "+str);
		File wowWTF = new File(path,"WTF");
		/**
		 * Does the name rename 
		 */
		if(wowInterface.renameTo(currentInterface) == true && newCurrentInterface.renameTo(wowInterface) == true && wowWTF.renameTo(currentWTF) == true && newCurrentWTF.renameTo(wowWTF) == true) {
			success = true;
			System.out.println("TRUE");
			current = str;
		}
		return success;
	}

}
